# SPDX-FileCopyrightText: 2010-2023 Blender Foundation
#
# SPDX-License-Identifier: GPL-2.0-or-later

from . import ui
from . import operator

def register():
    ui.register()
    operator.register()

def unregister():
    ui.unregister()
    operator.unregister()
